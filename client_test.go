package getswift

import (
	"encoding/json"
	. "github.com/onsi/gomega"
	"gitlab.com/foodchain/getswift/errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path/filepath"
	"testing"
	"time"
)

func stringRef(str string) *string {
	return &str
}

func boolRef(b bool) *bool {
	return &b
}

func timeRef(t time.Time) *time.Time {
	return &t
}

func floatRef(f float32) *float32 {
	return &f
}

func TestBookDeliverySendsCorrectRequest(t *testing.T) {
	RegisterTestingT(t)
	panicParse := func(timeStr string) time.Time {
		result, err := time.Parse(time.RFC3339, timeStr)
		if err != nil {
			t.Fatalf("Error parsing %s: %v", timeStr, err)
		}
		return result
	}
	successFilename := filepath.Join("testdata", "bookdelivery_success_response.json")
	successJson, err := ioutil.ReadFile(successFilename)
	requestedURL := ""
	requestedHost := ""
	var requestedJson interface{}
	if err != nil {
		t.Fatalf("Error loading success booking json fixture: %v", err)
	}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestedURL = r.URL.String()
		requestedHost = r.Host
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		if err := decoder.Decode(&requestedJson); err != nil {
			t.Fatalf("Error parsing received json: %v", err)
			w.WriteHeader(500)
			return
		}
		w.Write(successJson)
	}))
	defer ts.Close()

	config := &ClientConfig{
		APIKey:  "somekey",
		BaseURL: ts.URL,
	}
	client := NewClientWithConfig(config)
	deliveryBooking := DeliveryBooking{
		PickupDetail:  DeliveryBookingLocation{Address: "some address"},
		DropoffDetail: DeliveryBookingLocation{Address: "some other address"},
	}
	resp, err := client.BookDelivery(deliveryBooking)
	if err != nil {
		t.Fatalf("Error booking delivery: %v", err)
	}
	expectedResponse := &DeliveryBookingResponse{
		Quote: Quote{
			Created:    panicParse("2017-12-13T11:12:27.9592521+00:00"),
			Start:      panicParse("2017-12-13T11:12:27.9592521+00:00"),
			DistanceKm: 3.1,
			Fee: Fee{
				Cost:      1.0,
				CostCents: 100,
			},
			Pickup: PickupDropoffWindow{
				Time: TimeFrameWithAverage{
					Average:  panicParse("2017-12-13T11:12:27.9592521+00:00"),
					Earliest: panicParse("2017-12-13T11:12:27.9592521+00:00"),
					Latest:   panicParse("2017-12-13T11:12:27.9592521+00:00"),
				},
				Address: "sample string 1",
			},
			Dropoff: PickupDropoffWindow{
				Time: TimeFrameWithAverage{
					Average:  panicParse("2017-12-13T11:12:27.9592521+00:00"),
					Earliest: panicParse("2017-12-13T11:12:27.9592521+00:00"),
					Latest:   panicParse("2017-12-13T11:12:27.9592521+00:00"),
				},
				Address: "sample string 1",
			},
		},
		Delivery: Delivery{
			Created:   panicParse("2017-12-13T11:12:27.9592521+00:00"),
			ID:        "2fd8a3e3-0648-4539-997d-ada36e98c169",
			Reference: "sample string 3",
			PickupLocation: AddressDetails{
				Name:     "sample string 1",
				Address:  "sample string 2",
				Phone:    "sample string 3",
				Postcode: "sample string 4",
				Suburb:   "sample string 5",
			},
			DropoffLocation: AddressDetails{
				Name:     "sample string 1",
				Address:  "sample string 2",
				Phone:    "sample string 3",
				Postcode: "sample string 4",
				Suburb:   "sample string 5",
			},
			LastUpdated:   panicParse("2017-12-13T11:12:27.9602521+00:00"),
			CurrentStatus: "sample string 5",
			Driver: Driver{
				Identifier: "c4cbecd4-fe3c-468b-b83a-d6fc05699e22",
				Name:       "sample string 2",
				Phone:      "sample string 3",
				PhotoURL:   "sample string 4",
				Email:      "sample string 5",
			},
			Items: []string{
				"sample string 1",
				"sample string 2",
			},
			PickupTime: panicParse("2017-12-13T11:12:27.9602521+00:00"),
			DropoffTime: TimeFrame{
				EarliestTime: panicParse("2017-12-13T11:12:27.9612535+00:00"),
				LatestTime:   panicParse("2017-12-13T11:12:27.9612535+00:00"),
			},
			DeliveryInstructions: "sample string 6",
			CustomerReference:    "sample string 7",
			TrackingURLs: TrackingURLs{
				WWW: "sample string 1",
				API: "sample string 2",
			},
			ProofOfDelivery: ProofOfDelivery{
				SignatureURL: "sample string 1",
			},
			DriverTip:   8.0,
			DeliveryFee: 1.0,
			EstimatedDistance: Distance{
				Kilometres: 1.1,
				Miles:      2.1,
			},
		},
		Request: DeliveryBooking{
			Reference:            stringRef("sample string 1"),
			DeliveryInstructions: stringRef("sample string 2"),
			ItemsRequirePurchase: boolRef(false),
			Items: &[]DeliveryBookingItem{
				DeliveryBookingItem{
					Quantity:    1,
					SKU:         "sample string 2",
					Description: "sample string 3",
					Price:       4.0,
				},
				DeliveryBookingItem{
					Quantity:    1,
					SKU:         "sample string 2",
					Description: "sample string 3",
					Price:       4.0,
				},
			},
			PickupTime: timeRef(panicParse("2017-12-13T11:12:27.9612535+00:00")),
			PickupDetail: DeliveryBookingLocation{
				Name:        stringRef("sample string 1"),
				Phone:       stringRef("sample string 2"),
				Email:       stringRef("sample string 3"),
				Description: stringRef("sample string 4"),
				Address:     "sample string 5",
				AdditionalAddressDetails: &ExtraAddressDetails{
					StateProvince:  "sample string 1",
					Country:        "sample string 2",
					SuburbLocality: "sample string 3",
					Postcode:       stringRef("sample string 4"),
					Latitude:       5.1,
					Longitude:      6.1,
				},
			},
			DropoffWindow: &TimeFrame{
				EarliestTime: panicParse("2017-12-13T11:12:27.9612535+00:00"),
				LatestTime:   panicParse("2017-12-13T11:12:27.9612535+00:00"),
			},
			DropoffDetail: DeliveryBookingLocation{
				Name:        stringRef("sample string 1"),
				Phone:       stringRef("sample string 2"),
				Email:       stringRef("sample string 3"),
				Description: stringRef("sample string 4"),
				Address:     "sample string 5",
				AdditionalAddressDetails: &ExtraAddressDetails{
					StateProvince:  "sample string 1",
					Country:        "sample string 2",
					SuburbLocality: "sample string 3",
					Postcode:       stringRef("sample string 4"),
					Latitude:       5.1,
					Longitude:      6.1,
				},
			},
			CustomerFee:         floatRef(4.0),
			CustomerReference:   stringRef("sample string 5"),
			Tax:                 floatRef(1.0),
			TaxInclusivePrice:   boolRef(false),
			Tip:                 floatRef(1.0),
			DriverFeePercentage: floatRef(6.0),
			Webhooks: &[]DeliveryEventWebhook{
				DeliveryEventWebhook{
					EventName: "sample string 1",
					URL:       "sample string 2",
				},
				DeliveryEventWebhook{
					EventName: "sample string 1",
					URL:       "sample string 2",
				},
			},
			Template: stringRef("sample string 9"),
		},
	}
	Expect(resp).To(Equal(expectedResponse))
	Expect(requestedURL).To(Equal("/api/v2/deliveries"))
	u, _ := url.Parse(ts.URL)
	Expect(requestedHost).To(Equal(u.Host))
	Expect(requestedJson).To(Equal(map[string]interface{}{
		"apiKey": "somekey",
		"booking": map[string]interface{}{
			"dropoffDetail": map[string]interface{}{
				"address": "some other address",
			},
			"pickupDetail": map[string]interface{}{
				"address": "some address",
			},
		},
	}))
}

func TestReturnsErrorOn400(t *testing.T) {
	RegisterTestingT(t)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := map[string]interface{}{
			"message": "Some message",
			"code":    "somecode",
		}
		responseJson, err := json.Marshal(response)
		if err != nil {
			t.Fatalf("Error sending response json: %v", err)
			w.WriteHeader(500)
			return
		}
		w.WriteHeader(400)
		w.Write(responseJson)
	}))
	defer ts.Close()

	config := &ClientConfig{
		APIKey:  "somekey",
		BaseURL: ts.URL,
	}
	client := NewClientWithConfig(config)
	deliveryBooking := DeliveryBooking{
		PickupDetail:  DeliveryBookingLocation{Address: "some address"},
		DropoffDetail: DeliveryBookingLocation{Address: "some other address"},
	}
	_, err := client.BookDelivery(deliveryBooking)
	Expect(err).To(Equal(&errors.BadRequestError{
		Code:    "somecode",
		Message: "Some message",
	}))
}

func TestReturnsBadJsonResponseIfResponseNotParseableAndStatus200Or400(t *testing.T) {
	RegisterTestingT(t)

	//Test 200
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte("not{}json"))
	}))
	defer ts.Close()
	config := &ClientConfig{
		APIKey:  "somekey",
		BaseURL: ts.URL,
	}
	client := NewClientWithConfig(config)
	deliveryBooking := DeliveryBooking{
		PickupDetail:  DeliveryBookingLocation{Address: "some address"},
		DropoffDetail: DeliveryBookingLocation{Address: "some other address"},
	}
	_, err := client.BookDelivery(deliveryBooking)
	Expect(err).To(Equal(&errors.InvalidJSONResponse{
		RawBody: []byte("not{}json"),
	}))

	//Test 400
	ts = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(400)
		w.Write([]byte("not{}json"))
	}))
	defer ts.Close()
	config = &ClientConfig{
		APIKey:  "somekey",
		BaseURL: ts.URL,
	}
	client = NewClientWithConfig(config)
	_, err = client.BookDelivery(deliveryBooking)
	Expect(err).To(Equal(&errors.InvalidJSONResponse{
		RawBody: []byte("not{}json"),
	}))
}

func TestReturnsInternalErrorFor500(t *testing.T) {
	RegisterTestingT(t)

	//Test 200
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(500)
	}))
	defer ts.Close()
	config := &ClientConfig{
		APIKey:  "somekey",
		BaseURL: ts.URL,
	}
	client := NewClientWithConfig(config)
	deliveryBooking := DeliveryBooking{
		PickupDetail:  DeliveryBookingLocation{Address: "some address"},
		DropoffDetail: DeliveryBookingLocation{Address: "some other address"},
	}
	_, err := client.BookDelivery(deliveryBooking)
	Expect(err).To(Equal(&errors.InternalError{}))
}

func TestReturnsUnauthorizedErrorFor401(t *testing.T) {
	RegisterTestingT(t)

	//Test 200
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(401)
	}))
	defer ts.Close()
	config := &ClientConfig{
		APIKey:  "somekey",
		BaseURL: ts.URL,
	}
	client := NewClientWithConfig(config)
	deliveryBooking := DeliveryBooking{
		PickupDetail:  DeliveryBookingLocation{Address: "some address"},
		DropoffDetail: DeliveryBookingLocation{Address: "some other address"},
	}
	_, err := client.BookDelivery(deliveryBooking)
	Expect(err).To(Equal(&errors.UnauthorizedError{}))
}

func TestReturnsHTTPSRequiredErrorFor403(t *testing.T) {
	RegisterTestingT(t)

	//Test 200
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(403)
	}))
	defer ts.Close()
	config := &ClientConfig{
		APIKey:  "somekey",
		BaseURL: ts.URL,
	}
	client := NewClientWithConfig(config)
	deliveryBooking := DeliveryBooking{
		PickupDetail:  DeliveryBookingLocation{Address: "some address"},
		DropoffDetail: DeliveryBookingLocation{Address: "some other address"},
	}
	_, err := client.BookDelivery(deliveryBooking)
	Expect(err).To(Equal(&errors.HTTPSRequiredError{}))
}
