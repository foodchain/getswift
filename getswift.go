package getswift

import (
	"time"
)

type DeliveryBookingItem struct {
	Quantity    int     `json:"quantity,omitEmpty"`
	SKU         string  `json:"sku,omitEmpty"`
	Description string  `json:"description,omitEmpty"`
	Price       float32 `json:"price,omitEmpty"`
}

type ExtraAddressDetails struct {
	StateProvince  string  `json:"stateProvince,omitempty"`
	Country        string  `json:"country,omitempty"`
	SuburbLocality string  `json:"suburbLocality,omitempty"`
	Postcode       *string `json:"postcode,omitempty"`
	Latitude       float32 `json:"latitude,omitempty"`
	Longitude      float32 `json:"longitude,omitempty"`
}

type DeliveryBookingLocation struct {
	Name                     *string              `json:"name,omitempty"`
	Phone                    *string              `json:"phone,omitempty"`
	Email                    *string              `json:"email,omitempty"`
	Description              *string              `json:"description,omitempty"`
	Address                  string               `json:"address,omitempty"`
	AdditionalAddressDetails *ExtraAddressDetails `json:"additionalAddressDetails,omitempty"`
}

type TimeFrame struct {
	EarliestTime time.Time `json:"earliestTime,omitempty"`
	LatestTime   time.Time `json:"latestTime,omitempty"`
}

type DeliveryEventWebhook struct {
	EventName string `json:"eventName,omitempty"`
	URL       string `json:"url,omtiempty"`
}

type DeliveryBooking struct {
	Reference            *string                 `json:"reference,omitempty"`
	DeliveryInstructions *string                 `json:"deliveryInstructions,omitempty"`
	ItemsRequirePurchase *bool                   `json:"itemsRequirePurchase,omitempty"`
	Items                *[]DeliveryBookingItem  `json:"items,omitempty"`
	PickupTime           *time.Time              `json:"pickupTime,omitempty"`
	PickupDetail         DeliveryBookingLocation `json:"pickupDetail"`
	DropoffWindow        *TimeFrame              `json:"dropoffWindow,omitempty"`
	DropoffDetail        DeliveryBookingLocation `json:"dropoffDetail"`
	CustomerFee          *float32                `json:"customerFee,omitempty"`
	CustomerReference    *string                 `json:"customerReference,omitempty"`
	Tax                  *float32                `json:"tax,omitempty"`
	TaxInclusivePrice    *bool                   `json:"taxInclusivePrice,omitempty"`
	Tip                  *float32                `json:"tip,omitempty"`
	DriverFeePercentage  *float32                `json:"driverFeePercentage,omitempty"`
	Webhooks             *[]DeliveryEventWebhook `json:"webhooks,omitempty"`
	Template             *string                 `json:"template,omitempty"`
}

type Fee struct {
	Cost      float32 `json:"cost"`
	CostCents float32 `json:"costCents"`
}

type TimeFrameWithAverage struct {
	Earliest time.Time `json:"earliest"`
	Latest   time.Time `json:"latest"`
	Average  time.Time `json:"average"`
}

type PickupDropoffWindow struct {
	Time    TimeFrameWithAverage `json:"time"`
	Address string               `json:"address"`
}

type Quote struct {
	Created    time.Time           `json:"created"`
	Start      time.Time           `json:"start"`
	DistanceKm float32             `json:"distanceKm"`
	Fee        Fee                 `json:"fee"`
	Pickup     PickupDropoffWindow `json:"pickup"`
	Dropoff    PickupDropoffWindow `json:"dropoff"`
}

type AddressDetails struct {
	Name     string `json:"name"`
	Address  string `json:"address"`
	Phone    string `json:"phone"`
	Postcode string `json:"postcode"`
	Suburb   string `json:"suburb"`
}

type Driver struct {
	Identifier string `json:"identifier"`
	Name       string `json:"name"`
	Phone      string `json:"phone"`
	PhotoURL   string `json:"photoUrl"`
	Email      string `json:"email"`
}

type TrackingURLs struct {
	WWW string `json:"www"`
	API string `json:"api"`
}

type ProofOfDelivery struct {
	SignatureURL string `json:"signatureUrl"`
}

type Distance struct {
	Kilometres float32 `json:"kilometres"`
	Miles      float32 `json:"miles"`
}

type Delivery struct {
	Created              time.Time       `json:"created"`
	ID                   string          `json:"id"`
	Reference            string          `json:"reference"`
	PickupLocation       AddressDetails  `json:"pickupLocation"`
	DropoffLocation      AddressDetails  `json:"dropoffLocation"`
	LastUpdated          time.Time       `json:"lastUpdated"`
	CurrentStatus        string          `json:"currentStatus"`
	Driver               Driver          `json:"driver"`
	Items                []string        `json:"items"`
	PickupTime           time.Time       `json:"pickupTime"`
	DropoffTime          TimeFrame       `json:"dropoffTime"`
	DeliveryInstructions string          `json:"deliveryInstructions"`
	CustomerReference    string          `json:"customerReference"`
	TrackingURLs         TrackingURLs    `json:"trackingUrls"`
	ProofOfDelivery      ProofOfDelivery `json:"proofOfDelivery"`
	DriverTip            float32         `json:"driverTip"`
	DeliveryFee          float32         `json:"deliveryFee"`
	EstimatedDistance    Distance        `json:"estimatedDistance"`
}

type DeliveryBookingResponse struct {
	Quote    Quote           `json:"quote"`
	Delivery Delivery        `json:"delivery"`
	Request  DeliveryBooking `json:"request"`
}

type GetSwiftClient interface {
	// Book a delivery
	BookDelivery(DeliveryBooking) (*DeliveryBookingResponse, error)
}
