package getswift

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/Sirupsen/logrus"
	"gitlab.com/foodchain/getswift/errors"
	"io/ioutil"
	"net/http"
)

// Structure representing more complex configurations of the getswift client
// mostly useful for testing or customised logging
type ClientConfig struct {
	// The API key to make requests with
	APIKey string
	//The Base URL to make requests against
	BaseURL string
	// The http client to use
	HTTPClient *http.Client
	// The logger to use
	Logger *logrus.Logger
}

type clientImpl struct {
	apiKey     string
	baseURL    string
	httpClient *http.Client
	logger     *logrus.Logger
}

// For the most common usecases this is the best way to get a client
func NewClient(apiKey string) GetSwiftClient {
	config := &ClientConfig{
		APIKey:  apiKey,
		BaseURL: "https://app.getswift.co",
	}
	return NewClientWithConfig(config)
}

// Create a new client with the given config
func NewClientWithConfig(config *ClientConfig) GetSwiftClient {
	client := &clientImpl{
		apiKey:     config.APIKey,
		baseURL:    config.BaseURL,
		httpClient: config.HTTPClient,
		logger:     config.Logger,
	}
	if client.httpClient == nil {
		client.httpClient = &http.Client{}
	}
	if client.logger == nil {
		client.logger = logrus.New()
	}
	return client
}

type merchantDeliveryBooking struct {
	APIKey  string          `json:"apiKey"`
	Booking DeliveryBooking `json:"booking"`
}

type badRequestResponse struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (c *clientImpl) BookDelivery(booking DeliveryBooking) (*DeliveryBookingResponse, error) {
	c.logger.WithField("booking", booking).Info("Making booking request")
	merchantBooking := merchantDeliveryBooking{APIKey: c.apiKey, Booking: booking}
	reqBytes, err := json.Marshal(merchantBooking)
	if err != nil {
		c.logger.WithField("json_encoding_error", err).Error("Error encoding booking to JSON")
		return nil, err
	}
	url := fmt.Sprintf("%s%s", c.baseURL, "/api/v2/deliveries")
	reqReader := bytes.NewReader(reqBytes)

	reqLogger := c.logger.WithFields(logrus.Fields{
		"booking":     booking,
		"bookingJSON": string(reqBytes[:]),
		"url":         url,
	})
	reqLogger.Info("POSTing json to create delivery")

	resp, err := http.Post(url, "application/json", reqReader)
	if err != nil {
		reqLogger.WithField("error", err).Error("Error POSTing")
		return nil, err
	}

	var respLogger *logrus.Entry

	defer resp.Body.Close()
	bodyBytes, errReadingBody := ioutil.ReadAll(resp.Body)
	if errReadingBody != nil {
		// Don't return yet because we still want to log the status code specific error
		reqLogger.Error("Error reading response")
		respLogger = reqLogger
	} else {
		respLogger = reqLogger.WithField("responseBody", string(bodyBytes[:]))
	}

	if resp.StatusCode == 401 {
		respLogger.Debug("401 when posting")
		return nil, &errors.UnauthorizedError{}
	}

	if resp.StatusCode == 403 {
		respLogger.Debug("403 when POSTing")
		return nil, &errors.HTTPSRequiredError{}
	}

	if resp.StatusCode == 500 {
		respLogger.Error("500 when POSTing")
		return nil, &errors.InternalError{}
	}

	if errReadingBody != nil {
		return nil, errReadingBody
	}

	if resp.StatusCode == 400 {
		respLogger.Info("400 when POSTing")
		var response badRequestResponse
		if err := json.Unmarshal(bodyBytes, &response); err != nil {
			respLogger.WithField("jsonParseError", err).Error("Error parsing JSON from 400 response")
			return nil, &errors.InvalidJSONResponse{
				RawBody: bodyBytes,
			}
		}
		return nil, &errors.BadRequestError{Code: response.Code, Message: response.Message}
	}

	var response DeliveryBookingResponse
	if err := json.Unmarshal(bodyBytes, &response); err != nil {
		respLogger.WithField("jsonParseError", err).Error("Error parsing JSON from successfule response")
		return nil, &errors.InvalidJSONResponse{
			RawBody: bodyBytes,
		}
		return nil, err
	}
	respLogger.Info("Success")
	return &response, nil
}
