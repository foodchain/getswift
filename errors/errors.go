package errors

import (
	"fmt"
)

type BadRequestError struct {
	Code    string
	Message string
}

func (b *BadRequestError) Error() string {
	return b.Message
}

type UnauthorizedError struct{}

func (u *UnauthorizedError) Error() string {
	return "Unauthorized"
}

type HTTPSRequiredError struct{}

func (h *HTTPSRequiredError) Error() string {
	return "HTTPS Required"
}

type InternalError struct{}

func (i *InternalError) Error() string {
	return "Internal server error"
}

type InvalidJSONResponse struct {
	RawBody []byte
}

func (i *InvalidJSONResponse) Error() string {
	return fmt.Sprintf("Server responded with a 200 or 400 status code but the response body was not valid json. response body was %v", i.RawBody)
}
